//
//  Classes.swift
//  HW3
//
//  Created by Adam Denchfield on 1/27/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Foundation

precedencegroup ExpPrecedence {
    associativity: left
    higherThan: MultiplicationPrecedence
    
}

infix operator ** : ExpPrecedence // defining my own exponentiation operator

func **(base: Double, power: Double) -> Double { // creating the exponentiation operator
    let result = pow(base, power)
    return result
}

func **(base: Double, power: Int) -> Double { // creating the exponentiation operator
    let result : Double = pow(base, Double(power))
    return (result)
}

class Problems{

    
}

func writedata(filename: String, data: String){
    
    let path = "/home/adenchfi_mac/Desktop/" + filename
    // Set the contents
 //   let contents = "Here are my file's contents"
    
    do {
        // Write contents to file
        try data.write(toFile: path, atomically: false, encoding: String.Encoding.utf8)
    }
    catch let error as NSError {
        print("Ooops! Something went wrong: \(error)")
    }
}
    
func mysin(x: Double, N: Int) -> Double{
    /// a finite sum version of the sin(x) function using Taylor's expansion
    var sum : Double = 0
    for n in stride(from: 1, through: N, by: 1) {
        sum += (-1.0)**n * x**(2*n - 1) / Double(factorial(n: (2*n - 1)))
    }
    
    return sum
}

func factorial(n: Int) -> Int {
    if n >= 0 {
        return n == 0 ? 1 : n * factorial(n: n - 1)
    }
    else{
        return -1
    }
}

func mysin(x: Double, N: Int) -> Double{
    var sum: Double =  0
    
    for i in 1...N{
        sum = sum + Double((-1)**(i-1) * x**(2*i-1)) / Double(factorial(n: 2*i-1))
    }
    return sum
}

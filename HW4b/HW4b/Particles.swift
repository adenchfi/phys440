//
//  Particles.swift
//  HW4b
//
//  Created by Adam Denchfield on 2/3/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Foundation

class Particle{
    var KE: Double = 1 // just assuming it starts out with KE of 1
    var positions: [[Double]] = [[0.001,0.001,0.001]]
    var in_box: Bool = true
    
    func curr_position()->[Double]{
        let x = positions.last![0]
        let y = positions.last![1]
        let z = positions.last![2]
        return [x, y, z]
    }
    
    func update_position(x: Double, y: Double, z: Double)->Void{
        if (KE >= 0.0 && in_box == true){
            positions.append( [curr_position()[0] + x, curr_position()[1] + y, curr_position()[2] + z])
        }
        
    }
    
    
}

class Neutron: Particle {
    var mass = 939 // MeV/c^2

}

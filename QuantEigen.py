# A program that finds the energy eigenvalues of a quantum system with a potential V(x), also finds psi(x) using rk4 and a bisection algorithm

# right now, using mass of a particle m = 940 MeV, recall hbar*c = 197.33MeV-fm
# right now, well width = 20.0 fm
# well depth = 16 MeV, psi not normalized

from visual import * # will plot and update the graph

psigraph = display(x=0, y=0, width=600, height=300, title = 'Right & Left Wavefunctions')
Left_wavefunc = curve(x=list(range(502)), color=color.red)
Right_wavefunc = curve(x=list(range(997)), color=color.yellow)
eps = 1E-3 #the precision I want for now
n_steps = 501 # max number of steps?
E = -17.0 # initial energy guess, given the well depth is 16 MeV
h = 0.04 # stepsize; may instead just calculate from data points in a file
count_max = 100
Emax = 1.1*E
Emin = E/1.1 # E limits?
mass = 940 # MeV
hc = 197.33 # MeV-fm

def f(x, y, F, E): # evaluates a couple constants used in the rk4 algorithm
    F[0] = y[1]
    F[1] = -mass/(hc**2) * (E-V(x)) * y[0]

def V(x): # a test potential
    if (abs(x) < 10.): 
        return -16.0 # well depth
    else:
        return 0.0

def rk4(t, y, h, Neqs, E):
    # t = current step, y = wavefunction guess, h is the stepsize, E is current energy guess, Neqs is the number of equations we're matching, in this case 2
    F = zeros((Neqs), float)
    ydumb = zeros((Neqs), float) # dummy y variable
    k1 = zeros((Neqs), float) # the constants in the rk4 algorithm
    k2 = zeros((Neqs), float)
    k3 = zeros((Neqs), float)
    k4 = zeros((Neqs), float)
    
    f(t, y, F, E)  # get initial values
    for i in range(0, Neqs):
        k1[i] = h*F[i]
        ydumb[i] = y[i] + k1[i]/2.
    f(t + h/2., ydumb, F, E)
    for i in range(0, Neqs):
        k2[i] = h*F[i]
        ydumb[i] = y[i] + k2[i]/2.
    f(t + h/2., ydumb, F, E) # recall ydumb was just updated
    for i in range(0, Neqs):
        k3[i] = h*F[i]
        ydumb[i] = y[i] + k3[i]
    f(t+h, ydumb, F, E)
    for i in range(0, Neqs):
        k4[i] = h*F[i]
        y[i] = y[i] + (k1[i] + 2*(k2[i] + k3[i]) + k4[i])/6.0 # essential rk4 estimation

def diff(E, h): #does rk4 and brings right and left wavefunction guesses together until they meet, and calculates the difference between them
    y = zeros((2), float)
    i_match = n_steps//3 # matching radius, truncates the decimals after
    nL = i_match + 1
    y[0] = 1.E-15 # initial left wavefunction
    y[1] = y[0]*sqrt(-E * mass/(hc**2)) # initial derivative guess?
    for ix in range(0, nL + 1):
        x = h * (ix - n_steps/2) # n_steps / 2 because this is the left wavefunction, will reach the halfway point before being matched
        rk4(x, y, h, 2, E)
    left = y[1]/y[0] # log derivative
    y[0] = 1.E-15 # slope for even, reverse for odd
    y[1] = -y[0] * sqrt(-E * mass/(hc**2))
    for ix in range(n_steps, nL+1, -1): # going left from the right
        x = h*(ix + 1 - n_steps/2)
        rk4(x, y, -h, 2, E) # -h since we're decreasing steps here
    right = y[1]/y[0] # log derivative of right
    return ( (left - right)/(left + right) )

def plot(E, h): # repeat the integrations for the plot
    x = 0.
    n_steps = 1501 # no. of integration steps
    y = zeros((2), float)
    yL = zeros((2, 505), float)
    i_match = n_steps // 3 
    nL = i_match + 1
    y[0] = 1.E-15 # initial wavefunction guess
    y[1] = -y[0] * sqrt(-E * mass/(hc**2))
    for ix in range(0, nL + 1):
        yL[0][ix] = y[0]
        yL[1][ix] = y[1]
        x = h* (ix - n_steps/2)
        rk4(x, y, h, 2, E)

    y[0] = -1.E-8
    y[1] = -y[0] * sqrt(-E * mass/(hc**2))
    j = 0 # counter
    for ix in range(n_steps - 1, nL + 2, -1): # right wavefunction
        x = h * (ix + 1 - n_steps/2)
        rk4(x, y, -h, 2, E)
        Right_wavefunc.x[j] = 2.0*(ix + 1 - n_steps/2) - 500.0
        Right_wavefunc.y[j] = y[0] * 35e-2 + 200 # scale factor
#        print ("Right wavefunc: %d" % (y[0]))
        j += 1
    x = x-h
    Lnorm = y[0] / yL[0][nL]
    print (Lnorm)
    j = 0
    # Renormalization of left wavefunction and its derivative
    for ix in range(0, nL + 1):
        x = h * (ix + 1 - n_steps/2)
        y[0] = yL[0][ix]*Lnorm
        y[1] = yL[1][ix]*Lnorm
        Left_wavefunc.x[j] = 2.0*(ix + 1 - n_steps/2) - 500.0
        Left_wavefunc.y[j] = y[0]*35e-2  + 200 # scale factor
 #       print ("Left wavefunc: %d" % (y[0]))
        j += 1

for count in range(0, count_max + 1): 
    rate(1) # rate made slower so plot shows changes
    # Iteration loop
    E = (Emax + Emin)/2.0  # dividing the E range
    Diff = diff(E, h)
    if (diff(Emax, h) * Diff > 0):  # bisection algorithm
        Emax = E
    else:
        Emin = E
    if (abs(Diff) < eps):
        break # don't need to do the for loop anymore
    if count > 3: # first one's iteration is too irregular
        rate(4)
        plot(E, h)
    elabel = label(pos=(700, 400), text='E=', box=0)
    elabel.text = ('E=%13.10f' % E)
    ilabel = label(pos=(700, 600), text='istep=', box=0)
    ilabel.text = ('istep=%4s' % count)
elabel = label(pos=(700, 400), text='E=', box=0)
elabel.text = ('E=%13.10f' % E)
ilabel = label(pos=(700, 600), text='istep=', box=0)
ilabel.text = ('istep=%4s' % count)

print("Final eigenvalue E = ", E)
print("iterations, max = ", count)

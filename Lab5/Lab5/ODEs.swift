//
//  ODEs.swift
//  Lab5
//
//  Created by Adam Denchfield on 2/10/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Foundation


// want to do this until y(L) = E?
func Euler(dy_dx_init: Double, y_init: Double, x_vals: [Double], num_steps: Int)->Double{
    // initial dy/dx|x=x_init, y_init = y|x=x_init
    // x_final is the value where you'd like to approximate your function y
    // num_steps is how many steps you'd like to take along the way
    
    var ynew = 0.0
    
    var yvals: [Double] = [y_init]
    var dy_dx_vals: [Double] = [dy_dx_init]
    var stepsize: Double = 0
     // h = (b-a)/n, [a, b] are the bounds, n = number of steps you want to take
    
//    for n in 1...num_steps{ // we know how many steps we have; loop over them
//        stepsize = x_vals[n] - x_vals[n-1] // one step = size of step from one point to another
//        yvals.append( yvals[n-1] + stepsize * dy_dx_vals[n-1] ) // y_n = y_n-1 + h*f(y, x), f = dy/dx
//        dy_dx_vals[n] = (yvals[n] - yvals[n-1]) / stepsize // update the estimated derivative at each point
//    }
    
    return (x_vals, yvals) // return a 2-tuple of the x-values and y-values
}

func f(x: Double, psi: [Double], F: inout [Double],E: Double, V: [Double]){
    /// this is a function producing the relation between psi(x) and d(psi)/dx
    F[0] = psi[1]
    if (F[0].isNaN){
        print("Found NaN in f")
    }
    if (floor(x) == x){
        F[1] = -(E-V[Int(x)])*psi[0]
    }
    else {
        let lower_idx = floor(x)
        let higher_idx = ceil(x)
        
        let approx_V = linInterp(x1: lower_idx, x2: higher_idx, y1: V[Int(lower_idx)], y2: V[Int(higher_idx)], xdesired: x)
        
        F[1] = -(E-approx_V)*psi[0]
        
        //
    }
    
}



func linInterp(x1: Double, x2: Double, y1: Double, y2: Double, xdesired: Double)->Double{
    var ydesired = 0.0
    
    let slope = (y2 - y1) / (x2 - x1)
    ydesired = Double(y1 + slope*xdesired)
    return ydesired
}

func rk4(t: Int, y: inout [Double],h: Double,Neqs: Int,E: Double, V: [Double]){
    /// takes an x1 point t, a y1 point y, a stepsize h, the order of the ODE Neqs, and a parameter E
    /// produces the next point y2, using the rk4 algorithm
    var F  = [Double](repeating: 0.0, count: Neqs)
    var ydumb = [Double](repeating: 0.0, count: Neqs)
    var k1 = [Double](repeating: 0.0, count: Neqs)
    var k2 = [Double](repeating: 0.0, count: Neqs)
    var k3 = [Double](repeating: 0.0, count: Neqs)
    var k4 = [Double](repeating: 0.0, count: Neqs)
    let t1 = Double(t)
    f(x: t1, psi: y, F: &F,E: E, V: V)
    
    for i in 0...(Neqs-1){
        k1[i] = h*F[i]
        ydumb[i] = y[i] + k1[i]/2.0
        }
    
    f(x: t1 + 1/2.0, psi: ydumb, F: &F,E: E, V: V)
    
    for i in 0...(Neqs-1){
        k2[i] = h*F[i]
        ydumb[i] = y[i] + k2[i]/2.0
        }
    f(x: t1 + 1/2.0, psi: ydumb, F: &F,E: E, V: V)
    for i in 0...(Neqs-1){
        k3[i] =  h*F[i]
        ydumb[i] = y[i] + k3[i]
        }

    f(x: t1 + 1, psi: ydumb, F: &F,E: E, V: V);

    for i in 0...(Neqs-1){
        k4[i]=h*F[i]
        y[i]=y[i]+(k1[i]+2*(k2[i]+k3[i])+k4[i])/6.0
        }
    if (y[0].isNaN){
        /*print(k1[0])
        print(k2[0])
        print(k3[0])
        print(k4[0])
        print("Found a NaN")*/
    }
}

func diff(E: Double, xvals: [Double], Neqs: Int, n_steps: Int, V: [Double])->(Double, [Double]){ //does rk4 and brings right and left wavefunction guesses together until they meet, and calculates the difference between them. Returns the difference and the left wavefunction x-values, then y-values
    // in this case, right wavefunction will be ~0 for the right endpoint
    // Note: in this case, we are letting the matching be all the way at the right endpoint
    var y: [Double] = []
    var yL = [Double](repeating: 0.0, count: Neqs)

    var x = 0.0
    
    var h = 0.1 // will be the step-size that we alter
    let i_match = Int(n_steps) // what point at which the two wavefunctions should meet. Middle => n_steps / 2.
    
    let nL = i_match // the number of steps the left wavefunction should take
    
    yL[0] = 1e-16 // initial left wavefunction
    yL[1] = 9
    //yL[1] = yL[0]*sqrt(abs(E)) // initial derivative guess
    y.append(yL[0])
    
    
    
    for ix in 0...(nL-2){
        h = xvals[ix + 1] - xvals[ix]
        x = xvals[ix+1] // n_steps / 2 because this is the left wavefunction, will reach the halfway point before being matched
        Euler(dy_dx_init: yL[1], y_init: yL[0], h: h, Neqs: Neqs, E: E, V: V)
       // if (yL[0].isNaN){
       //     print("Found a NaN")
       // }
        y.append(yL[0])
    }
    let left = yL[0] // left wavefunction y-value at the right endpoint
    

    return ( left, y)
    }


func find_roots(tol: Double, xpoints: [Double], ypoints: [Double])->([Int], [Double]){
    /// given the x points and y points, approximate the roots of the equation
    var root_indxs: [Int] = []
    
    var roots: [Double] = [] // make an array to store the roots
    
    
    for index in 1...(ypoints.count - 1){ // loop through the elements of the xpoints
        // this is a clumsy way to find roots, has to go through the all the points.
        // if

        if (ypoints[index-1] < tol){ // if the previous y point is 0, it is a root
            roots.append(xpoints[index])
            root_indxs.append(index)
        }
            
        else if (ypoints[index-1] * ypoints[index] < tol){ // If two y-values multiplied together are negative, there is a root between them
            roots.append((xpoints[index] + xpoints[index])/2.0) // approximate the root as the middle of the two
            root_indxs.append(index)        }
        
        
    }
    
    return (root_indxs, roots)
}

//
//  Particles.swift
//  final_proj_test
//
//  Created by Adam Denchfield on 4/7/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Foundation


class System{
    
    var num_of_particles = 50 // default value
    
    var all_particles = [Particle]()
    
    init(num: Int) { // initializes array that contains all the particles
        for _ in 0...num_of_particles{
            all_particles.append(Particle())
        }
        num_of_particles = num
    }
    
    func get_KE()->Double{
        var KE = 0.0
        
        for particles in all_particles{
            KE += 0.5 * (pow(particles.my_velocity_x, 2) + pow(particles.my_velocity_y, 2))
        }
        
        return KE
    }
    
    
}

class Particle{
    
    var x_pos_history: [Double] = []
    var y_pos_history: [Double] = []
    var z_pos_history: [Double] = []
    
    var my_x_position = 0.0 // defaults
    var my_y_position = 0.0 // defaults, can change
    var my_z_position = 0.0
    
    var my_velocity_x = 1.0
    var my_velocity_y = 1.0
    var my_velocity_z = 1.0
    
    
    
}

func update(this_particle: Particle){
    
    this_particle.my_velocity_x += 1.0
    this_particle.x_pos_history.append(this_particle.my_x_position)
    
    this_particle.my_x_position += 1.0
    
    this_particle.my_y_position += 1.0 // this particle's y position = 1.0
    this_particle.my_velocity_y += 1.0 // y_velocity = 2.0
    
}

func update_all(system: System){
    var index = 0
    
    for each_particle in system.all_particles{
       
            
            print("Particle \(index)")
            
            print("x_pos: \(each_particle.my_x_position)")
            
            update(this_particle: each_particle)
            
            print("x_pos_new: \(each_particle.my_x_position)")
        
        index += 1
        
    }
    
    
    
}

func calc_R(particle: Particle)->Double{
    var R = 0.0
    
    R = sqrt(pow(particle.my_x_position, 2) + pow(particle.my_y_position, 2) + pow(particle.my_z_position, 2))
    
    return R
}

func calc_KE_system(system: System)->Double{
    var KE = 0.0
    
    var particles = system.all_particles
    
    for each_particle in particles{
        KE += calc_KE_particle(particle: each_particle)
    }
    
    return KE
}

func calc_KE_particle(particle: Particle)->Double{
    var KE = 0.0
    
    KE = pow(particle.my_velocity_x, 2) + pow(particle.my_velocity_y, 2) + pow(particle.my_velocity_z, 2)
    
    return KE
}

func Force_on_this_particle()->[Double]{
    
    
}

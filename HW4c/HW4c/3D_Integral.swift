//
//  3D_Integral.swift
//  HW4c
//
//  Created by Adam Denchfield on 2/10/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Foundation

let pi = 3.141592
let e = 2.718
let a0 = 1.0

infix operator **  {precedence 155} // defining my own exponentiation operator

func **(base: Double, power: Double) -> Double { // creating the exponentiation operator
    let result = pow(base, power)
    return result
}

func **(base: Double, power: Int) -> Double { // creating the exponentiation operator
    let result : Double = pow(base, Double(power))
    return (result)
}

func mean_val_MC(R: Double, points: Int)->Double{
    /// This function does mean value monte carlo integration on a function. I could make it more general, but for now it's designed to just work with the overlap integral.
        /// R is the interatomic spacing between two atoms, or otherwise half the size of the bounding box. points is the number of points to use
        var result = 0.0
        let max_x = R
        let max_y = R
        let max_z = R
        var randx = 0.0
        var randy = 0.0
        var randz = 0.0
        var r = 0.0
        var vol = 0.0
        var sum_of_func_evaluations = 0.0
        
        for _ in 1...points{
            /// generate a random r value
            randx = Double.getRandomNumber(lower: -max_x, upper: max_x)
            randy = Double.getRandomNumber(lower: -max_y, upper: max_y)
            randz = Double.getRandomNumber(lower: -max_z, upper: max_z)
  
            r = sqrt((randx**2) + (randy**2) + (randz**2))

            // sum of f(x) for many points
            sum_of_func_evaluations += overlap_f(r: r, R: R)
            
        }
        // volume of bounding box
        vol = (2*max_z)*(2*max_y)*(2*max_x)
        // mean value method of monte carlo integration
        result = vol * sum_of_func_evaluations / Double(points)
        
        return result
}
    
func overlap_f(r: Double, R: Double)->Double{
        var func_val = 0.0
        // r and R should be in terms of a0, eg r = 2 means r = 2*a0
        // I'm using the mid-point between the two atoms as the origin, hence the R/2 terms
        func_val = (1/pi) * (pow(a0, -3)) * pow(e, -R/2 - r/a0) * pow(e, -R/(2*a0))
        
        return func_val
    }
    

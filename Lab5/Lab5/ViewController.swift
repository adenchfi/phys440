//
//  ViewController.swift
//  Lab5
//
//  Created by Adam Denchfield on 2/10/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var prex: [Double] = []
        var preV: [Double] = []
        var appx: [Double] = []
        var appV: [Double] = []
        
        var x: [Double] = []
        var V: [Double] = []
        
        prex.append(0.0)
        preV.append(10e8)
        appx.append(10.0)
        
        for i in stride(from: 0.001, to: 10, by: 0.001){
            x.append(i)
            V.append(0) // testing square well
        }
        
        let xsize = x.count
        
        // enforce b boundary conditions
        /*
        for i in stride(from: x[0] - 20*(x[1]-x[0]), to: x[0], by: (x[1] - x[0])){
            prex.append(i)
            preV.append(10e8)
        }
        
        for i in stride(from: x[xsize-1], to: x[xsize-1] + 20*(x[xsize-1]-x[xsize-2]), by: x[xsize-1]-x[xsize-2]){
            appx.append(i)
            appV.append(10e8)
        }
        */
        x = prex + x + appV
        V = preV + V + preV
        
        
        let wfn: Wavefunctions = calc_wavefunction(max_steps: x.count, Neqs: 2, x: x, V: V)
        
        // Now to test, call the calcWavefunctions function, and plot it?
        
        // NEXT STEPS:
        // 0) Test that this actually makes correct-ish values; need to root find eigenenergies too
        // diff right now is returning the wrong thing
        // 1) Get plotting to work
        // 2) Add the points for the wavefunctions to the plot, potentially under different options
        // 3) Plot it
        
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    
    
    
    func testfilereading(){
        let file = "linPot.txt"
        var test_file: FileOperations = FileOperations()
        var x: [Double] = []
        var V: [Double] = []
        (x, V) = test_file.columns_2_Doubles(filename: file)
        
        for vals in x{
            print(vals)
        }
        for vals in V{
            print(vals)
        }
    }

}



//  ViewController.swift
//  Lab6
//
//  Created by Adam Denchfield on 3/3/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//
import Foundation
import Cocoa
import CorePlot

class ViewController: NSViewController, CPTScatterPlotDataSource, CPTAxisDelegate {
    
    @IBOutlet weak var hostingView: CPTGraphHostingView!
    @IBOutlet weak var Eigenvalue_Choices: NSPopUpButton!
    

    
    var scatterGraph : CPTXYGraph? = nil
    var contentArray = [plotDataType]()
        
    typealias plotDataType = [CPTScatterPlotField : Double]
    var dataForPlot = [plotDataType]()
    
    //var which_egn = 0 // may be used to go through which eigenvalue was chosen for plotting purposes
    var x = Double()
    var Quant = Hamiltonian(N: 100, file: "Potential")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Eigenvalue_Choices.removeAllItems() // remove the default items
        

        Quant.get_eigensystem()
        
        //print(Quant.Hmat)
        
        var minenergy = Quant.eigensystem[1].eigenvalue
        
        for eigenelements in Quant.eigensystem{
            Eigenvalue_Choices.addItem(withTitle: String(eigenelements.eigenvalue / minenergy))
        }
        
        
        
        // NOTE: HARMONIC OSCILLATOR DOESN'T APPEAR RIGHT?
        
        
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    func makePlot(xLabel: String, yLabel: String, xMin: Double, xMax: Double, yMin: Double, yMax: Double){
        
        // Create graph from theme
        let newGraph = CPTXYGraph(frame: .zero)
        newGraph.apply(CPTTheme(named: .darkGradientTheme))
        
        hostingView.hostedGraph = newGraph
        
        // Paddings
        newGraph.paddingLeft   = 10.0
        newGraph.paddingRight  = 10.0
        newGraph.paddingTop    = 10.0
        newGraph.paddingBottom = 10.0
        
        // Plot space
        let plotSpace = newGraph.defaultPlotSpace as! CPTXYPlotSpace
        plotSpace.allowsUserInteraction = true
        
        
        
        plotSpace.yRange = CPTPlotRange(location: NSNumber(value: yMin), length: NSNumber(value: (yMax-yMin)))
        plotSpace.xRange = CPTPlotRange(location: NSNumber(value: xMin), length: NSNumber(value: (xMax-xMin)))
        
        
        //Anotation
        
        let theTextStyle :CPTMutableTextStyle = CPTMutableTextStyle()
        
        theTextStyle.color =  CPTColor.white()
        
        let ann = CPTLayerAnnotation.init(anchorLayer: hostingView.hostedGraph!.plotAreaFrame!)
        
        ann.rectAnchor = CPTRectAnchor.bottom; //to make it the top centre of the plotFrame
        ann.displacement = CGPoint(x: 20.0, y: 20.0) //To move it down, below the title
        
        let textLayer = CPTTextLayer.init(text: xLabel, style: theTextStyle)
        
        ann.contentLayer = textLayer
        
        hostingView.hostedGraph?.plotAreaFrame?.addAnnotation(ann)
        
        let annY = CPTLayerAnnotation.init(anchorLayer: hostingView.hostedGraph!.plotAreaFrame!)
        
        annY.rectAnchor = CPTRectAnchor.left; //to make it the top centre of the plotFrame
        annY.displacement = CGPoint(x: 50.0, y: 30.0) //To move it down, below the title
        
        let textLayerY = CPTTextLayer.init(text: yLabel, style: theTextStyle)
        
        annY.contentLayer = textLayerY
        
        hostingView.hostedGraph?.plotAreaFrame?.addAnnotation(annY)
        
        
        
        
        // Axes
        let axisSet = newGraph.axisSet as! CPTXYAxisSet
        
        if let x = axisSet.xAxis {
            x.majorIntervalLength   = 1.0
            x.orthogonalPosition    = 0.0
            x.minorTicksPerInterval = 3
        }
        
        if let y = axisSet.yAxis {
            y.majorIntervalLength   = 0.5
            y.minorTicksPerInterval = 5
            y.orthogonalPosition    = 0.0
            y.delegate = self
        }
        
        // Create a blue plot area
        let boundLinePlot = CPTScatterPlot(frame: .zero)
        let blueLineStyle = CPTMutableLineStyle()
        blueLineStyle.miterLimit    = 1.0
        blueLineStyle.lineWidth     = 3.0
        blueLineStyle.lineColor     = .blue()
        boundLinePlot.dataLineStyle = blueLineStyle
        boundLinePlot.identifier    = NSString.init(string: "Blue Plot")
        boundLinePlot.dataSource    = self
        newGraph.add(boundLinePlot)
        
        let fillImage = CPTImage(named:"BlueTexture")
        fillImage.isTiled = true
        boundLinePlot.areaFill      = CPTFill(image: fillImage)
        boundLinePlot.areaBaseValue = 0.0
        
        // Add plot symbols
        let symbolLineStyle = CPTMutableLineStyle()
        symbolLineStyle.lineColor = .black()
        let plotSymbol = CPTPlotSymbol.ellipse()
        plotSymbol.fill          = CPTFill(color: .blue())
        plotSymbol.lineStyle     = symbolLineStyle
        plotSymbol.size          = CGSize(width: 10.0, height: 10.0)
        boundLinePlot.plotSymbol = plotSymbol
        
        self.dataForPlot = contentArray
        
        self.scatterGraph = newGraph
    }
    
    // MARK: - Plot Data Source Methods
    func numberOfRecords(for plot: CPTPlot) -> UInt
    {
        return UInt(self.dataForPlot.count)
    }
    
    func number(for plot: CPTPlot, field: UInt, record: UInt) -> Any?
    {
        let plotField = CPTScatterPlotField(rawValue: Int(field))
        
        if let num = self.dataForPlot[Int(record)][plotField!] {
            let plotID = plot.identifier as! String
            if (plotField! == .Y) && (plotID == "Green Plot") {
                return (num + 0.0) as NSNumber
            }
            else {
                return num as NSNumber
            }
        }
        else {
            return nil
        }
    }
    
    // MARK: - Axis Delegate Methods
    private func axis(_ axis: CPTAxis, shouldUpdateAxisLabelsAtLocations locations: NSSet!) -> Bool
    {
        if let formatter = axis.labelFormatter {
            let labelOffset = axis.labelOffset
            
            var newLabels = Set<CPTAxisLabel>()
            
            if let labelTextStyle = axis.labelTextStyle?.mutableCopy() as? CPTMutableTextStyle {
                for location in locations {
                    if let tickLocation = location as? NSNumber {
                        if tickLocation.doubleValue >= 0.0 {
                            labelTextStyle.color = .green()
                        }
                        else {
                            labelTextStyle.color = .red()
                        }
                        
                        let labelString   = formatter.string(for:tickLocation)
                        let newLabelLayer = CPTTextLayer(text: labelString, style: labelTextStyle)
                        
                        let newLabel = CPTAxisLabel(contentLayer: newLabelLayer)
                        newLabel.tickLocation = tickLocation
                        newLabel.offset       = labelOffset
                        
                        newLabels.insert(newLabel)
                    }
                }
                
                axis.axisLabels = newLabels
            }
        }
        
        return false
    }
    
    // this function will plot the chosen eigenvalue's associated wavefunction
    @IBAction func Choose_Eigenvalue(_ sender: Any) {
        let which_egn = Eigenvalue_Choices.indexOfSelectedItem
        let xvals = Quant.x
        let yvals = Quant.eigensystem[which_egn].eigenvector
        //print(yvals)
        for i in 0...xvals.count-1{
            let dataPoint: plotDataType = [.X: xvals[i], .Y: yvals[i]]
            contentArray.append(dataPoint)
            
        }
        
        makePlot(xLabel: "X", yLabel: "Y", xMin: -10, xMax: 10, yMin: -2, yMax: 2)
        contentArray.removeAll()
        
        // NEXT STEPS:
        // 
        // get the wavefunction x-values, y-values for each eigenvalue
        // we have the index of each eigenvalue right now; that will allow us to get what we need
        // then plot!
        
    
        
    }

    
}


//
//  ViewController.swift
//  HW3
//
//  Created by Adam Denchfield on 1/27/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var Prob1Label: NSTextField!
    
    @IBOutlet weak var Prob1TextField: NSTextField!
    @IBOutlet weak var NotesLabel: NSTextField!
    var message = ""
    var clicks = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func ProblemCalcButton(_ sender: AnyObject?) {
        // do all the exercises here
        
        
        switch clicks{
            case 0: Prob1a(a: 1, b: 1, c: 10**(-1)); break
            case 1: Prob1b_c(); break
            case 2: Prob2(); break
            case 3: Prob3(); break
            case 4: Prob4(); break
            case 5: Prob5(); break
            default: break
        }
        
        clicks += 1 // add a click when we press "Next Part"
        
    }
    
    @IBAction func PrevPartButton(_ sender: AnyObject?) {
        if (clicks == 0){
            // let the user know it's not doing anything
            Prob1Label.stringValue = "That isn't allowed; click Next Part"
        }
        else{
            clicks -= 1
            self.ProblemCalcButton(nil)
            
        }
        
    }
    
    func Prob1a(a: Double, b: Double, c: Double) -> Void{
        /// calculate the solutions x1, x1', x2, x2' for a quadratic formula and add a message to the text field
        
        let x1 = (-b + sqrt(b*b - 4*a*c))/(2*a)
        let x2 = (-b - sqrt(b*b - 4*a*c))/(2*a)
        let x1_ = (-2*c) / (b + sqrt(b*b - 4*a*c))
        let x2_ = (-2*c) / (b - sqrt(b*b - 4*a*c))
        let er1 = x1 - x1_
        let er2 = x2 - x2_
        
        message += "a: \(a) b:\(b) c: \(c) \nx1: \(x1) \nx2: \(x2) \nx1': \(x1_) \nx2': \(x2_)\n"
        
        if (er1 > er2) {
            message += "Second method is more precise\n"
        }
        else if (er1 < er2) {
            message += "First method is more precise\n"
        }
        else {
            message += "Both methods the same for these numbers.\n"
        }
        
        Prob1TextField.stringValue = message
        Prob1Label.stringValue = "Problem 1"
        
    }
    
    func Prob1b_c() -> Void{
        /// For a variety of values of c, with a and b as 1, this calculates the different x1, x2 values and machine precision is listed as a note.
        message = ""
        for n in 1...16 {
            Prob1a(a: 1,b: 1,c: 10.0**(Double(-n)))
        }
        NotesLabel.stringValue = "Machine precision is\n\(1.0.ulp)"
    }
    
    func Prob2() -> Void{
        var data = ""
        var finitesum1 : [Float] = []
        var finitesum2 : [Float] = []
        var finitesum3 : [Float] = []
        var tempsum1 : Float = 0
        var tempsum2a : Float = 0
        var tempsum2b : Float = 0
        var tempsum3 : Float = 0
        
        for N in stride(from: 1, through: 6, by: 1){
            for n in stride(from: 1, through: 10**N, by: 1){
                tempsum1 += Float((-1)**n * Double((n)/(n+1)))
                tempsum2a += Float(2*n / (2*n + 1))
                tempsum2b += Float((2*n - 1)/(2*n))
                tempsum3 += Float(1/((2*n)*(2*n + 1)))
            }
            finitesum1.append(tempsum1)
            finitesum2.append(tempsum2a - tempsum2b)
            finitesum3.append(tempsum3)
            tempsum1 = 0
            tempsum2a = 0
            tempsum2b = 0
            tempsum3 = 0
            data += "\(finitesum1[N-1]) \(finitesum2[N-1]) \(finitesum3[N-1])\n"
        }
        
        Prob1Label.stringValue = "Problem 2"
        writedata(filename: "HW3Prob2.txt", data: data)
    }
    
    func Prob3() -> Void{
        message = ""
        Prob1Label.stringValue = "Problem 3"
        Prob1TextField.stringValue = "Check your Desktop"
        
        var sum1: Double = 0
        var sum2: Double = 0
        
        // Set directory path for where I want the data
        var mydata = ""
        
        for N in stride(from: 1, through: 1000001, by: 100000) {
        sum1 = 0
        sum2 = 0
            for n in 1...N {
                sum1 += (1/Double(n))
            }
        
            for n in -N ... -1 {
                sum2 += (1/Double(-1*n))
            }
            
            // organize all the data into one string, since that's what the write() function requires
            mydata += "\(N) \(sum1) \(sum2)\n"
            
            
        }
        
        /// Now write to a file
        writedata(filename: "HW3Prob3.txt", data: mydata)
    }

    func Prob4() -> Void{
        message = ""
        Prob1Label.stringValue = "Problem 4"
        Prob1TextField.stringValue = "Problem 4 - actually 2.2.2 probs 1-6"
        
        let filename1 = "HW3P4x_0.1.txt"
        let filename2 = "HW3P4x_1.txt"
        let filename3 = "HW3P4x_10.txt"
        var mydata1 = ""
        var mydata2 = ""
        var mydata3 = ""
        
        let x = [0.1, 1, 10]
        // declaring the two arrays of known sizes; you need to do this otherwise a looot of weird errors come up
        var Jup = Array(repeating: Array(repeating: Double(), count: 25), count: 3)
        var Jdown = Array(repeating: Array(repeating: Double(), count: 25), count: 3)
        
        // making a temp variable because the XCode IDE can't seem to parse long single-line statements
        var temp: Double = 0
        
        // forward recursion relation
        for n in 0...2 {
            
            Jup[n][0] = sin(x[n])/x[n]
            Jup[n][1] = sin(x[n])/(x[n]**2)-cos(x[n])/x[n]
            for l in 1...23 {
                temp = ((Double(l*2+1))/x[n])*Jup[n][l] - Jup[n][l-1]
                Jup[n][l+1] = temp
            }
        }
        
        for n in 0...2 {
            // for the reverse recursion, we can start with any two values; will scale later
            Jdown[n][24] = 1
            Jdown[n][23] = 1
            for l in -23...(-1) {
                temp = (Double(-l*2+1)/x[n])*Jdown[n][-l]
                temp = temp - Jdown[n][-l + 1]
                Jdown[n][-1*l-1] = temp
            }
            // rescaling, since Swift doesn't seem to have a way to divide everything by a scalar...
            for l in -23...(-1) {

                Jdown[n][-1*l-1] *= Jup[n][0]/Jdown[n][0]
            }
            
        }
        
        // gather up the bessel data in one file to be used/plotted elsewhere
        for l in 0...23 {
            mydata1 += "\(Jup[0][l]) \(Jdown[0][l])\n"
            mydata2 += "\(Jup[1][l]) \(Jdown[1][l])\n"
            mydata3 += "\(Jup[2][l]) \(Jdown[2][l])\n"
        }
        // write the data to 3 different files, one for x = 0.1, x=1, x=10
        writedata(filename: filename1, data: mydata1)
        writedata(filename: filename2, data: mydata2)
        writedata(filename: filename3, data: mydata3)
    }
    
    func Prob5()->Void{
         
        Prob1Label.stringValue = "Problem 5"

        var x = 0.5
        var N : Int = 1
        var del_sum: Double = 0.001 // just a beginning value
        var curr_sum: Double = 0
        var past_sum: Double = 0
        
        var mywrite1 = ""
        var mywrite2 = ""
        var mywrite3 = ""
        let filename1 = "P5x-0.2"
        let filename2 = "P5x-0.5"
        let filename3 = "P5x-0.8"
        
        while abs(del_sum) >= 10**(-8){
            past_sum = mysin(x: x, N: N)
            N = N+1
            curr_sum = mysin(x: x, N: N)
            del_sum = curr_sum - past_sum
        }
        
        message += "Using x = 0.5, sin(x) converged within 10^(-8) for N = \(N).\n"
        
        // now do x = 3pi + 0.001; examine the individual terms and their cancellation at n = 5
        x = 3 * Double.pi - 0.001
        
        message += "Using x = 3pi - 0.001, N = 5, sin(x) = \(mysin(x: x, N: 5))\n"
        message += "Recall sin (x + 2pi) = sin(x); for x = 3pi - .001, \nsin(3pi - 001) = sin(pi - .001)\n"
        
        message += "sin(x) recalculated with the above lines = \(mysin(x: x - 2 * Double.pi, N: 5))\n"
        message += "x = 1, sin(x) = \(mysin(x: 1, N: N)) Off By: \(sin(1) - mysin(x: 1, N: N)) \n"
        message += "x = 10, sin(x) = \(mysin(x: 10, N: N)) Off By: \(sin(10) - mysin(x: 10, N: N))\n"
        message += "x = 100, sin(x) = \(mysin(x: 100, N: N)) Off By: \(sin(100) - mysin(x: 100, N: N))\n"
        N = 1
        while N < 10{
           
            
            mywrite1 += "\(N ) \(sin(0.2) - mysin(x: 0.2, N: N))\n"
            mywrite2 += "\(N) \(sin(0.5) - mysin(x: 0.5, N: N))\n"
            mywrite3 += "\(N) \(sin(0.8) - mysin(x: 0.8, N: N))\n"
            N += 1
        }
        writedata(filename: filename1, data: mywrite1)
        writedata(filename: filename2, data: mywrite2)
        writedata(filename: filename3, data: mywrite3)
        Prob1TextField.stringValue = message
    }
    
}

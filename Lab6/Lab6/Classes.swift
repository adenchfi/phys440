//
//  Classes.swift
//  Lab6
//
//  Created by Adam Denchfield on 3/3/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Foundation
import Accelerate

struct eigensystem_el{
    var eigenvalue = 0.0
    var eigenvector: [Double] = []
}

class Hamiltonian {
    var Hmat = [[Double]]()
    var x: [Double] = []
    var V: [Double] = []
    var basis_size = 10 // using an initial size
    var write_file_name = ""
    var eigen_basis_coefficients = [[Double]]() // will be the coefficients/weights returned by our LinAlg eigenvector solver
    var eigen_functions = [[Double]]() // will be the coefficients * the respective basis_function
    var eigenenergies: [Double] = [] // will be solved for! Should be the energies for the whole system
    
    var eigensystem: [eigensystem_el] = []
    //
    
    init(N: Int, file: String){ // getting the x-values, V-values from a text file. Or can initialize myself. Gets the size of the basis
        (x, V) = filereading(filename: file)
        basis_size = N
        write_file_name = file + "_Hmat.txt"
        // error checking; need at least as many data points as basis functions?
        if (basis_size > x.count){
            basis_size = x.count
        }
    }
    
    func getwfn(x: [Double], V: [Double])->Wavefunctions{
        var wfn = Wavefunctions(xlower: x.first!, xupper: x.last!, xvals: x, size: basis_size)
        return wfn
    }
    
    // constructing our initial H-matrix
    func construct_Hmat(wfn: Wavefunctions, V: [Double]){
        
        for i in 0..<basis_size{ // nested for loop to create the Hmat matrix
            Hmat.append([]) // append a new row
            for j in 0..<basis_size{
                // append a new column-element for each row; in this case, <psi_m|H|psi_n>
                (Hmat[i]).append(inner_H(wfn: wfn, V: V, idx1: i, idx2: j)) // row, column style
            }
            
        }
        array_file_writing(filename: write_file_name, data: Hmat)
    }
    
    
    // used in constructing the elements of Hmat
    // check if bugs for other potentials?
    func inner_H(wfn: Wavefunctions, V: [Double], idx1: Int, idx2: Int)->Double{ // calculates the expectation value  <psi_m|H|psi_n>
        var first_term = 0.0 // will be E_n if m=n, 0 otherwise
        var second_term = 0.0 // will be the numerical integral of psi_n*V*psi_m over all the x
        var psin_V_psim: [Double] = []
        
        if (idx1 == idx2){
            first_term = (wfn.basis_energies)[idx1]
        } // else it remains 0
        
        
        for i in 0...((wfn.xpoints).count - 1){
            psin_V_psim.append( ((wfn.basis_functions)[idx1])[i] * V[i] * (wfn.basis_functions[idx2])[i])
        }
        
        second_term = trapezoid(x: wfn.xpoints, y: psin_V_psim) // integrate!
        //print("First term: ", (first_term))
        //print("Second term: ", (second_term))
        return (first_term + second_term) // return the two values!
        
    }
    
    func get_eigensystem(){
        
        var wfn = getwfn(x: x, V: V)
        construct_Hmat(wfn: wfn, V: V)
        var system: LinAlg = LinAlg(inputarr: Hmat, squaresize: basis_size)
        (eigen_basis_coefficients, eigenenergies) = system.eigensolve()
        
        for i in 0..<basis_size{ // loop over the elements and multiply them!
            eigen_functions.append([]) // add the new i-th column
            for egn in 0..<x.count{
                
                // eigen_function[i][j] = its weight * the basis function there
                //eigen_functions[i].append(eigen_basis_coefficients[i][egn] * wfn.basis_functions[i][egn])
                
                eigen_functions[i].append(1 * wfn.basis_functions[i][egn])
            } // basis functions is fine, but eigen functions is weird...?
            eigensystem.append(eigensystem_el())
            eigensystem[i].eigenvalue = eigenenergies[i]
            eigensystem[i].eigenvector = eigen_functions[i]
        }
        //eigensystem = [eigenenergies, eigen_functions]
        eigensystem.sort(by: { $0.eigenvalue < $1.eigenvalue })
        
    }
    
    // TO DO:
    // find: eigenvectors/eigenvalues of Hmat, which gives us the coefficients of our original basis eigenfunctions to be used in making these new eigenfunctions for this particular potential
    // eigenvalues are the eigenenergies of the new eigenfunctions
    // construct our eigenfunctions (for each x value, add sum(basis_psi(x)) and plot them!
    
}

func trapezoid(x: [Double], y: [Double])->Double{ // general trapezoidal rule for variable step size
    var result = 0.0
    let hstart = x[1] - x[0]
    let hend = x[(x.count-1)] - x[(x.count-2)]
    var hmid = 0.0
    
    result += (hstart*y.first! + hend*y.last!)/2.0
    
    for i in 1...(x.count-3){ // since I do the last element above
        hmid = x[i+1] - x[i]
        result += (hmid*y[i])
    }
    
    return result
}

class Wavefunctions {
    // each energy has an eigenfunction; these eigenfunctions are collections of x/y points defining them
    let pi = 3.141592
    let h = 1.0 // letting h be 1
    let m = 1.0 // letting mass be 1
    
    var Xmin = 0.0
    var Xmax = 1.0
    var L = 1.0
    var k = 0.0 // wavenumber
    
    var basis_energies: [Double] = [] // will be the particle in a box

    var basis_functions = [[Double]]() // will be the particle in a box basis functions

    
    var xpoints: [Double]
    
    var basis_size = 50 // how many basis functions to use by default
    
    
    init(xlower: Double, xupper: Double, xvals: [Double], size: Int){ // get the lower, upper bounds of system
        Xmin = xlower
        Xmax = xupper
        L = Xmax - Xmin // calculate L used in defining basis energies and eigenfunctions
        xpoints = xvals
        basis_size = size
        for i in 0..<(basis_size){
            basis_energies.append(pow(Double(i)*h/(L),2) / (2*m)) // E_n = (nh/L)^2 * (1/2m)
            
            basis_functions.append([])
            for x in xpoints{
                k = Double(i)*pi/L
                (basis_functions[i]).append(sin(k*x))  // psi_n = sin(n*pi*x/L)
            }
        }
        
        
    }
    
    
    
}


class FileOperations {
    
    func columns_2_Doubles(filename: String)->([Double], [Double]){
        
        var column1: [Double] = []
        var column2: [Double] = []
        
        var temp: [String] = []
        var text = ""
        
        let file = filename //this is the file. we will write to and read from it
        
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(file)
            
            //reading
            do {
                text = try String(contentsOf: path, encoding: String.Encoding.utf8)
            }
            catch {/* error handling here */}
        }
        
        let textArr = text.components(separatedBy: .newlines)
        
        for rows in textArr{
            temp = (rows.components(separatedBy: "\t"))
            if (temp[0] != ""){
                column1.append(Double(temp[0])!)
                column2.append(Double(temp[1])!)
            }
        }
        
    return (column1, column2)

        
    }
    
    
    func writedata(filename: String, data: String){
        
        let path = "/home/adenchfi_mac/Desktop/" + filename
        // Set the contents
        //   let contents = "Here are my file's contents"
        
        do {
            // Write contents to file
            try data.write(toFile: path, atomically: false, encoding: String.Encoding.utf8)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    
}

class LinAlg{
    
    var flatArray: [Double] = []
    var size = Int32(1) // N
    
    var error : Int32 = 0
    var lwork = Int32(-1)
    // Real parts of eigenvalues
    var wr: [Double] = []
    // Imaginary parts of eigenvalues
    var wi: [Double] = []
    // Left eigenvectors
    var vl: [Double] = []
    // Right eigenvectors
    var vr: [Double] = []
    var workspaceQuery: Double = 0.0 // size of workspace needed
    var workspace: [Double] = []
    
    var eigenvectors: [[Double]] = [[]] // (:, j) is the j-th eigenvector; double check if bugs?
    var eigenvalues: [Double] = []
    
    init(inputarr: [[Double]], squaresize: Int){
        size = Int32(squaresize)
        flatArray = pack2dArray(arr: inputarr, rows: Int(size), cols: Int(size))
        
        wr = [Double](repeating: 0.0, count: Int(size))
        wi = [Double](repeating: 0.0, count: Int(size))
        vl = [Double](repeating: 0.0, count: Int(size*size))
        vr = [Double](repeating: 0.0, count: Int(size*size))
        
        
    }
    
    
    func eigensolve()->([[Double]], [Double]){
    /// eigensolve takes our fortran column-major array initialized in init() and solves for the eigenvectors and eigenvalues, returning them in a 2-tuple.
        dgeev_(UnsafeMutablePointer(mutating: ("N" as NSString).utf8String), UnsafeMutablePointer(mutating: ("V" as NSString).utf8String), &size, &flatArray, &size, &wr, &wi, &vl, &size, &vr, &size, &workspaceQuery, &lwork, &error)
        
        workspace = [Double](repeating: 0.0, count: Int(workspaceQuery))
        lwork = Int32(workspaceQuery)
        
        dgeev_(UnsafeMutablePointer(mutating: ("N" as NSString).utf8String), UnsafeMutablePointer(mutating: ("V" as NSString).utf8String), &size, &flatArray, &size, &wr, &wi, &vl, &size, &vr, &size, &workspace, &lwork, &error)
        
        /* To Save Memory dgeev returns a packed array if complex */
        /* Must Unpack Properly to Get Correct Result
         
         VR is DOUBLE PRECISION array, dimension (LDVR,N)
         If JOBVR = 'V', the right eigenvectors v(j) are stored one
         after another in the columns of VR, in the same order
         as their eigenvalues.
         If JOBVR = 'N', VR is not referenced.
         If the j-th eigenvalue is real, then v(j) = VR(:,j),
         the j-th column of VR.
         If the j-th and (j+1)-st eigenvalues form a complex
         conjugate pair, then v(j) = VR(:,j) + i*VR(:,j+1) and
         v(j+1) = VR(:,j) - i*VR(:,j+1). */
        
        // NOTE: wr contains real parts of eigenvalues, wi contains imaginary parts
        // NOTE: vr(:, j) is the j-th eigenvector accompanying the j-th eigenvalue; ie, the j-th column of vr
        // NOTE: for now, since we should only be dealing with real eigenvalues/vectors, will assume we only have real eigenvectors. Later on add in error checking for imaginary parts? -- added a checker for seeing if imag part is significant
        
        if (error == 0){
            
            for index in 0..<wi.count{
                if (wi[index] > 10E-6)
                    {print("Imaginary Eigenvalue found")}
                eigenvalues.append(wr[index]) // this is the next eigenvalue
                eigenvectors.append([]) // add a new empty eigenvector
                
                for j in 0..<size{
                    // for the (index)-th eigenvalue, this is the eigenvector
                    eigenvectors[index].append((vr[Int(index)*(Int(size))+Int(j)]))
                }
                
            }
        }
        return (unpack2dArray(arr: flatArray, rows: Int(size), cols: Int(size)), eigenvalues)
        //return (eigenvectors, eigenvalues) ??? uncomment later
        
    }
    
    /// pack2DArray
    /// Converts a 2D array into a linear array in FORTRAN Column Major Format
    ///
    /// - Parameters:
    ///   - arr: 2D array
    ///   - rows: Number of Rows
    ///   - cols: Number of Columns
    /// - Returns: Column Major Linear Array
    func pack2dArray(arr: [[Double]], rows: Int, cols: Int) -> [Double] {
        var resultArray = Array(repeating: 0.0, count: rows*cols)
        for Iy in 0...cols-1 {
            for Ix in 0...rows-1 {
                let index = Iy * rows + Ix
                resultArray[index] = arr[Ix][Iy]
            }
        }
        return resultArray
    }
    
    /// unpack2DArray
    /// Converts a linear array in FORTRAN Column Major Format to a 2D array in Row Major Format
    ///
    /// - Parameters:
    ///   - arr: Column Major Linear Array
    ///   - rows: Number of Rows
    ///   - cols: Number of Columns
    /// - Returns: 2D array
    func unpack2dArray(arr: [Double], rows: Int, cols: Int) -> [[Double]] {
        var resultArray = [[Double]](repeating:[Double](repeating:0.0 ,count:rows), count:cols)
        for Iy in 0...cols-1 {
            for Ix in 0...rows-1 {
                let index = Iy * rows + Ix
                resultArray[Ix][Iy] = arr[index]
            }
        }
        return resultArray
    }

}


func filereading(filename: String)->([Double],[Double]){
    var test_file: FileOperations = FileOperations()
    var x: [Double] = []
    var V: [Double] = []
    (x, V) = test_file.columns_2_Doubles(filename: filename)
    
    /*for vals in x{
        print(vals)
    }
    for vals in V{
        print(vals)
    }*/
    return (x, V)
}

func array_file_writing(filename: String, data: [[Double]]){
    var test_file: FileOperations = FileOperations()
    var writedata = ""
    
    for row in 0...data.count-1{
        for column in 0...data.count-1{
            writedata += "\(((data[row][column])*10000).rounded() / 10000)\t\t"
            
        }
        writedata += "\n"
    }
    
    test_file.writedata(filename: filename, data: writedata)
    
}

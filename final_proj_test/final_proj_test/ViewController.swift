//
//  ViewController.swift
//  final_proj_test
//
//  Created by Adam Denchfield on 4/7/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var num_of_particles = 100
        
        var my_system = System(num: num_of_particles)
        
        var timesteps = 10
        
        for timestep in 0..<timesteps{
            update_all(system: my_system)
        }
        
        
        
        print("Kinetic Energy: \(my_system.get_KE())")
        
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    
    

}



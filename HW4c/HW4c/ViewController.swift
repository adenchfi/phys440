//
//  ViewController.swift
//  HW4c
//
//  Created by Adam Denchfield on 2/10/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var Rlabel: NSTextField!
    
    @IBOutlet weak var Exact_Field: NSTextField!
    @IBOutlet weak var Exact_Label: NSTextField!
    @IBOutlet weak var Note_Label: NSTextField!
    @IBOutlet weak var R_field: NSTextField!
    @IBOutlet weak var Result_Field: NSTextField!
    
    @IBAction func CalcOverlapButton(_ sender: Any) {
        if ((R_field.doubleValue) < 0.0 || (R_field.doubleValue) > 10.0){
            Note_Label.stringValue = "Please enter a correct value and try again."
        }
        else{
            var num_result = 0.0
            var exact_result = 0.0
            var R = R_field.doubleValue
            let points = 100000
            num_result = mean_val_MC(R: R, points: points)
            
            Result_Field.doubleValue = num_result
            
            exact_result = (1 + R + (1/3)*R*R)*pow(e, -R)
            Exact_Field.doubleValue = exact_result
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}


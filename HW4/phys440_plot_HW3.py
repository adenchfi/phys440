import numpy as np
import matplotlib.pyplot as plt
import sys
import math
x1,y1=np.genfromtxt(sys.argv[1],dtype=float,unpack=True)
z1 = y1 # will become the relative difference
label1, = plt.plot(x1,y1, color='b',label='Error', linewidth=0.5)#drawstyle='steps'
plt.xlabel('(N)')
plt.ylabel('Difference')
plt.title('Pg. 43 Pr 6 x = 0.8')
plt.show()

//
//  ViewController.swift
//  HW4b
//
//  Created by Adam Denchfield on 2/3/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var Title_Label: NSTextField!
    @IBOutlet weak var Loss_15Pc_Label: NSTextField!
    @IBOutlet weak var Loss_10Pc_Label: NSTextField!
    @IBOutlet weak var Loss_10Pc_Field: NSTextField!
    
    @IBOutlet weak var displayview: DrawingView!
    @IBOutlet weak var Loss_15Pc_Field: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func CalcWalk_Button(_ sender: Any) {
        // want a lot of neutrons to pass through, do steps until they escape or lose all their energy
        let num_of_neutrons = 10000
        var escaped_neutrons = 0
        var del_x: Double = 0
        var del_y: Double = 0
        var del_z: Double = 0
        var len: Double = 0.0
        var point = (xPoint: 0.0, yPoint: 0.0, e_mx: 2.0, color: "Red")
        
        var neutrons = [Neutron]()
        
        
        for _ in 0...num_of_neutrons{
            neutrons.append(Neutron())
        }
        
        for n in neutrons{
            n.update_position(x: 2.5, y: 2.5, z: 4)
            n.positions.remove(at: 0)

        }
        
        for _ in 0...10{

            
            for n in neutrons{
                del_x = Double.getRandomNumber(lower: -1, upper: 1)
                del_y = Double.getRandomNumber(lower: -1, upper: 1)
                del_z = Double.getRandomNumber(lower: -1, upper: 1)
                
                
                len = sqrt(pow(del_x, 2.0) + pow(del_y, 2.0) + pow(del_z, 2.0))
                del_x /= len
                del_y /= len
                del_z /= len
                
                n.update_position(x: del_x, y: del_y, z: del_z)
                n.KE -= 0.1
                check_if_in_box(neutron: n)
            }
        }
        
        for n in neutrons{
            if (n.in_box == false){
                escaped_neutrons += 1
            }
        }
        
        for n in neutrons[1...2]{
            displayview.shouldIDrawPoints = true
            for pos in n.positions{
                point.xPoint = pos[0] * Double(displayview.frame.width) / 5.0
                point.yPoint = pos[1] * Double(displayview.frame.height) / 5.0
                point.color = "Blue"
                
                displayview.addPoint(xPointa:point.xPoint, yPointb: point.yPoint, radiusPointc: point.e_mx, colord: point.color)
            }
            
            
        }
        
        displayview.tellGuiToDisplay()
        Loss_10Pc_Field.stringValue = "\(100 * escaped_neutrons / num_of_neutrons)"
        
    }
    @IBAction func CalcWalkButton2(_ sender: Any) {
        // want a lot of neutrons to pass through, do steps until they escape or lose all their energy
        let num_of_neutrons = 10000
        var escaped_neutrons = 0
        var del_x: Double = 0
        var del_y: Double = 0
        var del_z: Double = 0
        var len: Double = 0.0
        var point = (xPoint: 0.0, yPoint: 0.0, e_mx: 2.0, color: "Red")
        
        var neutrons = [Neutron]()
        
        
        for _ in 0...num_of_neutrons{
            neutrons.append(Neutron())
        }
        
        for n in neutrons{
            n.update_position(x: 2.5, y: 2.5, z: 4)
            n.positions.remove(at: 0)
            
        }
        
        for _ in 0...10{
            
            
            for n in neutrons{
                del_x = Double.getRandomNumber(lower: -1, upper: 1)
                del_y = Double.getRandomNumber(lower: -1, upper: 1)
                del_z = Double.getRandomNumber(lower: -1, upper: 1)
                
                
                len = sqrt(pow(del_x, 2.0) + pow(del_y, 2.0) + pow(del_z, 2.0))
                del_x /= len
                del_y /= len
                del_z /= len
                
                n.update_position(x: del_x, y: del_y, z: del_z)
                n.KE -= 0.15
                check_if_in_box(neutron: n)
            }
        }
        
        for n in neutrons{
            if (n.in_box == false){
                escaped_neutrons += 1
            }
        }
        
        for n in neutrons[1...2]{
            displayview.shouldIDrawPoints = true
            for pos in n.positions{
                point.xPoint = pos[0] * Double(displayview.frame.width) / 5.0
                point.yPoint = pos[1] * Double(displayview.frame.height) / 5.0
                point.color = "Blue"
                displayview.addPoint(xPointa:point.xPoint, yPointb: point.yPoint, radiusPointc: point.e_mx, colord: point.color)
            }
            
            
        }
        
        displayview.tellGuiToDisplay()
        Loss_15Pc_Field.stringValue = "\(100 * escaped_neutrons / num_of_neutrons)"
        
    }
    
    func check_if_in_box(neutron: Neutron){
        
        let x = neutron.positions.last![0]
        let y = neutron.positions.last![1]
        let z = neutron.positions.last![2]
        
        if ((x >= 5.0) || (x <= 0.0))
        {
            neutron.in_box = false
        }
        if ( (y>=5.0) || (y <= 0.0) ){
            neutron.in_box = false
        }
        if ( (z >= 5.0) || (z <= 0.0) ){
            neutron.in_box = false
        }
        
        
    }

}


//
//  Quantum.swift
//  Lab5
//
//  Created by Adam Denchfield on 2/10/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

let m = 500.0 // MeV/c^2 of an electron
let hc = 197.33 // 197 MeV-fm
let massscale = m/pow(hc, 2)

import Foundation

class FileOperations {

    func columns_2_Doubles(filename: String)->([Double], [Double]){
        
        var column1: [Double] = []
        var column2: [Double] = []
        
        var temp: [String] = []
        var text = ""
        
        let file = filename //this is the file. we will write to and read from it
    
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(file)
            
            //reading
            do {
                text = try String(contentsOf: path, encoding: String.Encoding.utf8)
            }
            catch {/* error handling here */}
        }
        
        let textArr = text.components(separatedBy: "\n")
        
        for rows in textArr{
            temp = (rows.components(separatedBy: "\t"))
            column1.append(Double(temp[0])!)
            column2.append(Double(temp[1])!)
        }
        
        return (column1, column2)
        
    }
}

// test potential
//func V(x: Double)->Double{
    /// ideally this will instead read a file. If the x asked for is inside the list of x values, return the V(x) specified in the file. Otherwise, if the x value asked for is between some two x-values, find the two x-values and V(x) values and interpolate. Will need to implement a binary search algorithm then that returns the two x-values.
    
//    return 0.5*pow(x, 2) // harmonic oscillator potential
//}

class Wavefunctions {
    // each energy has an eigenfunction; these eigenfunctions are collections of x/y points defining them
    var Emax = -1.0
    var Emin = 0.0
    var Xmin = 0.0
    var Xmax = 1.0
    var energies: [Double] = []
    var eigenenergies: [Double] = []
    var psix = [[]]
    var psiy = [[]]
    var eigenfunctions = [[]] // will be the psiy that have endpoints that match the boundary conditions
    
    
}

func calc_wavefunction(max_steps: Int, Neqs: Int, x: [Double], V: [Double])->Wavefunctions{
    // will read in x, V(x) values from a user-supplied file, approximate the potential as eventually being particle-in-a-box, where we treat a potential high enough as infinity - a cutoff value
    // will loop over x values, producing a single energy E? Loop over energies E?
    
    // V(x) = 0 and V(x) = 0.5 * k * x^2 can be test wavefunctions
    
    // let user define the energy spacing and the Emax and Emin.
        // for now, hard code it
    var wavefuncs: Wavefunctions = Wavefunctions()
    wavefuncs.Emax = 25.0
    var Emax_local = 0.0
    var Emin_local = 0.0
    var E_local = 0.0
    let Emin = 0.0 // eV
    let E_step = 0.05
    var Diff = 2.0 // ratio of difference/addition of left wavefunction and required energy; should be 1 if they match
    let tol = 1e-24 // tolerance necessary
    var curr_steps = 0 // number of steps executed so far
    let Energies = stride(from: Emin, through: wavefuncs.Emax, by: E_step)
    var wfn: [Double] = []
    var eigenindxs: [Int] = []
    var eigenroots: [Double] = []
    var bdy_wfn_guesses: [Double] = []
    var ens: [Double] = []
    for energy in Energies{
        ens.append(energy)
        curr_steps = 0
        E_local = energy
       /*
        while (abs(Diff) > tol){ // Diff = 1 means the left and right are the same
            curr_steps += 1
            Emax_local = E_local - E_step
            Emin_local = E_local + E_step
            if (curr_steps >= max_steps) {break} // get out of the while loop
            
            (Diff, wfn) = diff(E: E_local, xvals: x, Neqs: Neqs, n_steps: max_steps)
            if (diff(E: Emax_local, xvals: x, Neqs: Neqs, n_steps: max_steps).0 * Diff > 0){
                Emax_local = energy
            }
            else {
                Emin_local = energy
            }
            
            E_local = (Emax_local + Emin_local) / 2.0
        }
        */
        
        Emax_local = E_local - E_step/2.0
        Emin_local = E_local + E_step/2.0

        
        (Diff, wfn) = diff(E: E_local, xvals: x, Neqs: Neqs, n_steps: max_steps, V: V)
        bdy_wfn_guesses.append(Diff)
        //if (curr_steps < max_steps) { // then we converged, store energy/wavefunction
        //    wavefuncs.eigenenergies.append(E_local)
        //    (wavefuncs.psi).append( [x, wfn] ) // append the wavefunction (x,y) values here...
        //}
        
        wavefuncs.energies.append(E_local)
        (wavefuncs.psix).append(x)
        (wavefuncs.psiy).append(wfn)
        
        // else: didn't converge, there isn't an eigenenergy in that range, do nothing
        
        
        
    }
    var closest = 1.0
    for els in bdy_wfn_guesses{
        if (abs(els) < closest){
            closest = els
        }
    }

    (eigenindxs, eigenroots) = find_roots(tol: tol,xpoints: ens, ypoints: bdy_wfn_guesses)
    for i in 0...(eigenroots.count-1){
        (wavefuncs.eigenfunctions).append((wavefuncs.psiy)[eigenindxs[i]])
        (wavefuncs.eigenenergies).append((wavefuncs.energies)[eigenindxs[i]])
    }
    (wavefuncs.eigenfunctions).remove(at: 0)
         // for e in E{
    //      psi(L) = 0
    
    //      Euler method to produce psi(L)
    
    //      for x in xvals_read{
    //          #dostuff
    //      }
    //      psi(L) = result (Euler())
    // }
    return wavefuncs
}

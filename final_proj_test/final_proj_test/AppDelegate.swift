//
//  AppDelegate.swift
//  final_proj_test
//
//  Created by Adam Denchfield on 4/7/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}


//
//  AppDelegate.swift
//  HW4b
//
//  Created by Adam Denchfield on 2/3/17.
//  Copyright © 2017 Adam Denchfield. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

